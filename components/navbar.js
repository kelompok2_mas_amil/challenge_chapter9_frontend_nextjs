import axios from "axios";
import Link from "next/link";
import React, { useEffect, useRef, useState } from "react";
import styles from "../styles/Navbar.module.css";
import "../pages/_app";

function NavBar(){
    // const [currentUser,setUser] = useState({ token: "", username: "" });
    // const token = sessionStorage.getItem("accessToken");
    // const id = sessionStorage.getItem("id");
    // const url = 'http://localhost:8080/api/user/'+id;
  
    // useEffect(() => {
  
    //   if (token != null) {
    //     axios.get(url, {
    //       headers: {
    //         'Authorization': token
    //       }
    //     }).then(res => {
    //       setUser({ token: token, username: res.data.username });
    //     });
    //   }
      
    // }, [])
  return(
    <>
      <nav className={`${styles.navbar} navbar navbar-expand-lg fixed-top`}>
        <Link href="/#">
          <a className={`${styles.navbarBrand} navbar-brand`}>LOGO</a>
        </Link>

        <button className={`navbar-toggler`} type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className={`navbar-toggler-icon`}></span>
        </button>
        
        <div className={`collapse navbar-collapse justify-content-between`} id="navbarNav">
          <ul className={`${styles.leftNavbar} navbar-nav mr-auto`}>
            <li className={`${styles.navItem} nav-item`}>
              <Link href="/">
                <a className={`${styles.navLink} nav-link`}>
                  HOME
                </a>
              </Link>
            </li>
            <li className={`${styles.navItem} nav-item`}>
              <Link href="/games/list">
                <a className={`${styles.navLink} nav-link`}>
                  GAME LIST
                </a>
              </Link>
            </li>
            <li className={`${styles.navItem} nav-item`}>
              <Link href="/#">
                <a className={`${styles.navLink} nav-link`}>
                  CONTACT
                </a>
              </Link>
            </li>
            <li className={`${styles.navItem} nav-item`}>
              <Link href="#">
                <a className={`${styles.navLink} nav-link`}>
                  ABOUT US
                </a>
              </Link>
            </li>
          </ul>

          <ul className={`${styles.rightNavbar} navbar-nav ml-auto`}>  
            <li className={`${styles.navItem} nav-item`}>
              <Link href="login">
                <a className={`${styles.navLink} nav-link`}>
                  LOGIN
                </a>
              </Link>
            </li>
            <li className={`${styles.navItem} nav-item`}>
              <Link href="register">
                <a className={`${styles.navLink} nav-link`}>
                  REGISTER
                </a>
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    </>
  )
}
export default NavBar;