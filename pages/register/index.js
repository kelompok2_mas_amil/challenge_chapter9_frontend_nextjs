import Link from 'next/link';
import styles from '../../styles/Register.module.css';

export default function Register() {
    return (
        <>
            <div className={styles.backgroundRegister}></div>
            <div className={styles.input}>
                <div className={styles.inputContainer}>
                    <h1>REGISTER</h1>
                    
                    <div className={styles.inputBox}>
                        <input type="email" required="required" name="email" onChange={(value)=>setEmail(value.target.value)}/>
                        <span>Email</span>
                    </div>
                    
                    <div className={styles.inputBox}>
                        <input type="text" required="required" name="username"onChange={(value)=>setUsername(value.target.value)}/>
                        <span>Username</span>
                    </div>
                    
                    <div className={styles.inputBox}>
                        <input type="password" required="required" name="password"onChange={(value)=>setPassword(value.target.value)}/>
                        <span>Password</span>
                    </div>
                    
                    <div className={styles.inputBox}>
                        <input type="text" name="bio" onChange={(value)=>setBio(value.target.value)}/>
                        <span>Bio</span>
                    </div>

                    <div className={styles.inputBox}>
                        <input type="text" name="city" onChange={(value)=>setCity(value.target.value)}/>
                        <span>City</span>
                    </div>

                    <div className={styles.inputBox}>
                        <input type="text" name="socialMedia" onChange={(value)=>setSocialMedia(value.target.value)}/>
                        <span>Social Media URL</span>
                    </div>
                    
                    <div className={styles.inputBox}>
                        <h5 className={styles.error}>&nbsp;</h5>
                        <button className={styles.buttonLogin} onClick={()=>handleSubmit()}>Register</button>
                        <Link className={styles.loginNav} href="/">Back</Link>
                    </div>
                </div>   
            </div>
        </>
    )
}