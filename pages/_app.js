import Head from "next/head";
import '../styles/globals.css';

import '../styles/landing-page.css';
import '../styles/games-list.css';
import '../styles/games-detail.css';
import '../styles/games.css';
import 'bootstrap/dist/css/bootstrap.css';

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>

      <Component {...pageProps} />
    </>
  )
}

export default MyApp
