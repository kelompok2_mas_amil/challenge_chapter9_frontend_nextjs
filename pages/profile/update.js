import Link from "next/link";
import styles from "../../styles/UpdateProfile.module.css";

export default function ProfileUpdate() {
    return (
        <>
        <div className={styles.main}>
        <div className='row container-height' >
            <div className='col-lg-6 col-md-6 m-auto' >
                <div className='container' >
                    <h1 className='text-center'>Update Profile</h1>
                        <form >
                            <fieldset>
                                <div className='form-group' >
                                    <label htmlFor='exampleInputEmail'>Email</label>
                                    <input
                                        type="email"
                                        name='email'
                                        className='form-control'
                                        // value={currentUser.email}
                                        required="required"
                                        placeholder="{currentUser.email}"
                                        onChange={(value)=>setEmail(value.target.value)}
                                    />
                                </div>
                                <div className='form-group' >
                                    <label htmlFor='exampleInputEmail'>Password</label>
                                    <input
                                        type="password"
                                        name='password'
                                        className='form-control'
                                        // value={currentUser.email}
                                        required="required"
                                        placeholder="minimum 6 karakter"
                                        onChange={(value)=>setPassword(value.target.value)}
                                    />
                                </div>
                                <div className='form-group'>
                                    <label htmlFor='exampleInputName' >Fullname</label>
                                    <input
                                        type="text"
                                        name='fullname'
                                        className='form-control'
                                        // value={currentUser.fullname}
                                        required="required"
                                        placeholder="{currentUser.fullname}"
                                        onChange={(value)=>setFullname(value.target.value)}
                                    />
                                </div>
                                <div className='form-group' >
                                    <label htmlFor='exampleInputBio'>Bio</label>
                                    <input
                                        type="text"
                                        name='bio'
                                        className='form-control'
                                        // value={currentUser.bio}
                                        required="required"
                                        placeholder="{currentUser.bio}"
                                        onChange={(value)=>setBio(value.target.value)}
                                    />
                                </div>
                                <div className='form-group' >
                                    <label htmlFor='exampleInputCity'>City</label>
                                    <input
                                        type="text"
                                        name='city'
                                        className='form-control'
                                        // value={currentUser.city}
                                        required="required"
                                        placeholder="{currentUser.city}"
                                        onChange={(value)=>setCity(value.target.value)}
                                    />
                                </div>
                                <div className='form-group' >
                                    <label htmlFor='exampleInputSosial'>Social Media Url</label>
                                    <input
                                        type="text"
                                        className='form-control'
                                        name='social_media_url'
                                        // value={currentUser.social_media_url}
                                        required="required"
                                        placeholder="{currentUser.social_media_url}"
                                        onChange={(value)=>setSocial_Media_Url(value.target.value)}
                                    />
                                </div>
                                <br />
                                <h5 style={{color:"white"}}  className="error"></h5>
                            
                            </fieldset>
                        </form>
                        <div className="d-flex gap-3">
                            <Link href="/#">
                                <a className='btn btn-primary'>Update your profile</a>
                            </Link>
                            <Link href="/">
                                <a className='btn btn-warning'>Back</a>
                            </Link>
                        </div>
                </div>
            </div>
        </div>
        </div>
        </>
    )
}