import Link from "next/link";
import styles from "../../styles/Profile.module.css";

export default function Profile() {
  return (
    <>
      <div className={styles.background}>
          <div className={styles.profileContainer}>
            <div className={styles.userData}>
            <div>
                  <h2 className={styles.dataTitle}>USER ID</h2>
                  <p>1</p>
                </div>
                <div>
                  <h2 className={styles.dataTitle}>USERNAME</h2>
                  <p>anton</p>
                </div>
                <div>
                  <h2 className={styles.dataTitle}>FULLNAME</h2>
                  <p>Pak Anton</p>
                </div>
            </div>
            <div className={styles.userBio}>
                <div>
                  <h2 className={styles.dataTitle}>BIO</h2>
                  <p>Hello world</p>
                </div>
            </div>
            
            <div className={styles.userData}>
                <div>
                  <h2 className={styles.dataTitle}>TOTAL SCORE</h2>
                  <p>0</p>
                </div>
                <button className={styles.editButton}>EDIT</button>
            </div>
          </div>
      </div>
    </>
  )
}