import React, { useEffect, useState } from 'react'
import axios from 'axios'
import Link from 'next/link';
import Navbar from '../components/navbar.js';
import Image from 'next/image.js';

const LandingPage = () => {
  return (
    <>
      <Navbar />
      <section id="main-screen">
        <div className="main-background"></div>
        <div className="main-screen">
          <div className="grid-item-container">
            <h1 className="grid-item">PLAY TRADITIONAL GAME</h1>
            <p className="grid-item">Selamat Datang 
            {/* {currentUser.token != "" ? currentUser.username.charAt(0).toUpperCase() + currentUser.username.slice(1) : ''} */}
            </p>
            <Link href="/games/list">
                <button className="grid-item">Play Now</button>
            </Link>
            {/* <button className="grid-item" onClick={() => window.location = "/List"}>Play Now</button> */}
          </div>
        </div>
      </section>
      {/* token " {!token && ("*/}
      <>
      <section id="the-games">
        <div className="the-games-background"></div>
        <div className="the-games-text">
          <div className="the-games-item-container">
            <p>What&apos;s so special?</p>
            <h1>THE GAMES</h1>
          </div>
        </div>
        <div className="the-games-carousel">
          <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
            <ol className="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div className="carousel-inner">
              <div className="carousel-item active">
                <Image className="d-block w-100" width="480px" height="240px" src="/images/rockpaperstrategy-1600.jpg" alt="First slide" />
              </div>
              <div className="carousel-item">
                <Image className="d-block w-100" width="480px" height="240px" src="/images/rockpaperstrategy-1600.jpg" alt="Second slide" />
              </div>
              <div className="carousel-item">
                <Image className="d-block w-100" width="480px" height="240px" src="/images/rockpaperstrategy-1600.jpg" alt="Third slide" />
              </div>
            </div>
            <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span className="carousel-control-prev-icon" aria-hidden="true"></span>
              <span className="sr-only">Previous</span>
            </a>
            <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span className="carousel-control-next-icon" aria-hidden="true"></span>
              <span className="sr-only">Next</span>
            </a>
          </div>
        </div>
      </section>

          <section id="features">

            <div className="features-background"></div>
            <div className="features-text">
              <div className="features-item-container">
                <div className="features-text-container">
                  <p>What&apos;s so special?</p>
                  <h1>FEATURES</h1>
                  <div className="features-desc">
                    <div className="traditional-games">
                      <h3>TRADITIONAL GAMES</h3>
                      <p>If you miss your childhood, we provide many traditional games here</p>
                    </div>
                    <div className="traditional-games">
                      <h3>GAME SUITS</h3>
                    </div>
                    <div className="traditional-games">
                      <h3>TBD</h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </section>
          <section id="requirement">
            <div className="requirement-background" alt=""></div>
            <div className="requirement-container">
              <p>Can My Computer Run This Game?</p>
              <h1>System <br />Requirements</h1>
              <div className="specification">
                <div className="os">
                  <h3>OS :</h3>
                  <p>Windows 7 64-bit only</p>
                  <p>No OSX support at this time</p>
                </div>
                <div className="processor">
                  <h3>Processor:</h3>
                  <p>Inter Core Duo @ 2.4 GHZ or</p>
                  <p>AMD Athlon X2 @ 2.8 GHZ</p>
                </div>
                <div className="memory">
                  <h3>Memory:</h3>
                  <p>4GB RAM</p>
                </div>
                <div className="storage">
                  <h3>Storage:</h3>
                  <p>8GB Available Space</p>
                </div>
                <div className="graphic">
                  <h3>Graphics:</h3>
                  <p>NVIDIA GeForce GTX 660 2GB or</p>
                  <p>AMD Radeon HD 7850 2GB DirectX11 (Shader Model 5)</p>
                </div>
              </div>
            </div>
          </section>
          <section id="top-scores">
            <div className="top-scores-background"></div>
            <div className="top-scores-desc">
              <div>
                <h1>TOP SCORES</h1>
                <p>This top scores from various games provided in this platform</p>
                <button>See Now</button>
              </div>
            </div>
            <div className="top-scores-review">
              <div className="review-card review-1">
                <div className="info-profile">
                  <Image className="profile profile-item" src="/images/evan.png" alt="" width='68px' height='68px' />
                  <div className="profile-desc profile-item ">
                    <p className="profile-name">Evan Lahti</p>
                    <p className="profile-job">PC Gamer</p>
                    <div className="profile-twitter">
                      <svg width="19" height="15" viewBox="0 0 19 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path opacity="0.3" d="M18.3981 2.41449C17.7231 2.70013 17.0072 2.88765 16.2741 2.97084C17.0365 2.53297 17.623 1.84099 17.899 1.01526C17.1731 1.42694 16.3789 1.71699 15.5508 1.87286C14.8519 1.15947 13.8744 0.755389 12.8519 0.757085C10.8105 0.757085 9.15361 2.33909 9.15361 4.29061C9.15361 4.5679 9.18627 4.83816 9.25021 5.09623C6.17639 4.94862 3.45216 3.54236 1.62787 1.404C1.30934 1.92529 1.12764 2.53297 1.12764 3.18076C1.12764 4.40623 1.78024 5.48805 2.77231 6.12188C2.18494 6.10468 1.61046 5.95309 1.09706 5.67983V5.7249C1.09706 7.43659 2.37189 8.86495 4.06278 9.19027C3.5182 9.3312 2.94722 9.35187 2.39305 9.25072C2.86384 10.6549 4.22998 11.6764 5.8475 11.7061C4.5821 12.6524 2.98781 13.2184 1.25483 13.2184C0.960153 13.2181 0.66574 13.2015 0.373047 13.1689C2.00943 14.1713 3.95365 14.7571 6.04185 14.7571C12.8438 14.7571 16.5627 9.37118 16.5627 4.70056C16.5627 4.54779 16.5627 4.39502 16.5513 4.24335C17.2749 3.74295 17.8998 3.12372 18.3969 2.41449H18.3981Z" fill="white" />
                      </svg>

                    </div>
                  </div>
                </div>
                <p className="comment">&quot;One of my gaming highlights of the year&quot;</p>
                <p className="date">June,18 2021</p>
              </div>
              <div className="review-card review-2">
                <div className="info-profile">
                  <Image className="profile profile-item" src="/images/jada.png" alt="" width='68px' height='68px' />
                  <div className="profile-desc profile-item ">
                    <p className="profile-name">Jada Griffin</p>
                    <p className="profile-job">Nerdreactor</p>
                    <div className="profile-twitter">
                      <svg width="19" height="15" viewBox="0 0 19 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path opacity="0.3" d="M18.3981 2.41449C17.7231 2.70013 17.0072 2.88765 16.2741 2.97084C17.0365 2.53297 17.623 1.84099 17.899 1.01526C17.1731 1.42694 16.3789 1.71699 15.5508 1.87286C14.8519 1.15947 13.8744 0.755389 12.8519 0.757085C10.8105 0.757085 9.15361 2.33909 9.15361 4.29061C9.15361 4.5679 9.18627 4.83816 9.25021 5.09623C6.17639 4.94862 3.45216 3.54236 1.62787 1.404C1.30934 1.92529 1.12764 2.53297 1.12764 3.18076C1.12764 4.40623 1.78024 5.48805 2.77231 6.12188C2.18494 6.10468 1.61046 5.95309 1.09706 5.67983V5.7249C1.09706 7.43659 2.37189 8.86495 4.06278 9.19027C3.5182 9.3312 2.94722 9.35187 2.39305 9.25072C2.86384 10.6549 4.22998 11.6764 5.8475 11.7061C4.5821 12.6524 2.98781 13.2184 1.25483 13.2184C0.960153 13.2181 0.66574 13.2015 0.373047 13.1689C2.00943 14.1713 3.95365 14.7571 6.04185 14.7571C12.8438 14.7571 16.5627 9.37118 16.5627 4.70056C16.5627 4.54779 16.5627 4.39502 16.5513 4.24335C17.2749 3.74295 17.8998 3.12372 18.3969 2.41449H18.3981Z" fill="white" />
                      </svg>
                    </div>
                  </div>
                </div>
                <p className="comment">&quot;The next big thing in the world of streaming and survival games&quot;</p>
                <p className="date">July,10 2021</p>
              </div>
              <div className="review-card review-3">
                <div className="info-profile">
                  <Image className="profile profile-item" src="/images/aaron.png" alt="" width='68px' height='68px' />
                  <div className="profile-desc profile-item ">
                    <p className="profile-name">Aaron Williams</p>
                    <p className="profile-job">Uproxx</p>
                    <div className="profile-twitter">
                      <svg width="19" height="15" viewBox="0 0 19 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path opacity="0.3" d="M18.3981 2.41449C17.7231 2.70013 17.0072 2.88765 16.2741 2.97084C17.0365 2.53297 17.623 1.84099 17.899 1.01526C17.1731 1.42694 16.3789 1.71699 15.5508 1.87286C14.8519 1.15947 13.8744 0.755389 12.8519 0.757085C10.8105 0.757085 9.15361 2.33909 9.15361 4.29061C9.15361 4.5679 9.18627 4.83816 9.25021 5.09623C6.17639 4.94862 3.45216 3.54236 1.62787 1.404C1.30934 1.92529 1.12764 2.53297 1.12764 3.18076C1.12764 4.40623 1.78024 5.48805 2.77231 6.12188C2.18494 6.10468 1.61046 5.95309 1.09706 5.67983V5.7249C1.09706 7.43659 2.37189 8.86495 4.06278 9.19027C3.5182 9.3312 2.94722 9.35187 2.39305 9.25072C2.86384 10.6549 4.22998 11.6764 5.8475 11.7061C4.5821 12.6524 2.98781 13.2184 1.25483 13.2184C0.960153 13.2181 0.66574 13.2015 0.373047 13.1689C2.00943 14.1713 3.95365 14.7571 6.04185 14.7571C12.8438 14.7571 16.5627 9.37118 16.5627 4.70056C16.5627 4.54779 16.5627 4.39502 16.5513 4.24335C17.2749 3.74295 17.8998 3.12372 18.3969 2.41449H18.3981Z" fill="white" />
                      </svg>
                    </div>
                  </div>
                </div>
                <p className="comment">&quot;Snoop Dogg Playing The Wildly Entertaining &apos;SOS&apos; Is Ridiculous&quot;</p>
                <p className="date">December,24 2018</p>
              </div>
            </div>
          </section>

          <section id="newsletters">

            <div className="newsletter-background"></div>
            <div className="newsletter-text-container">
              <div>
                <p>Want to stay in touch?</p>
                <h1>NEWSLETTER <br />SUBSCRIBE</h1>
                <p>In order to start receiving our news, all you have to do is enter your email address. Everythin else will be taken care of by us.We will send you emails containing information about game.We don&apos;t spam</p>
                <div className="newsletter-input">
                  <div className="email">
                    <form className="" action="">
                      <label htmlFor="emailInput">Your Email Address</label>
                      <input className="" id="emailInput" type="email" value="" placeholder="youremail@binar.co.id" />
                    </form>

                    <button>Subscribe Now</button>
                  </div>
                </div>
              </div>
            </div>
            <div className="footer">
              <div className="p">
                <div className="footer-top">
                  <div className="footer-nav">
                    <a className="footer-top-item footer-nav-item" href=""><p>MAIN</p></a>
                    <a className="footer-top-item footer-nav-item" href=""><p>ABOUT</p></a>
                    <a className="footer-top-item footer-nav-item" href=""><p>GAME FEATURES</p></a>
                    <a className="footer-top-item footer-nav-item" href=""><p>SYSTEM REQUIREMENT</p></a>
                    <a className="footer-top-item footer-nav-item" href=""><p>QUOTES</p></a>
                  </div>
                  <div className="social-media">
                    <a className="footer-top-item " href="">
                      <svg width="10" height="20" viewBox="0 0 10 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path className="facebook" fillRule="evenodd" clipRule="evenodd" d="M6.65382 19.1726H2.56282V9.67859H0.517822V6.40559H2.56282V4.44159C2.56282 1.77259 3.71782 0.183594 6.99582 0.183594H9.72582V3.45659H8.02082C6.74282 3.45659 6.65882 3.91459 6.65882 4.76859L6.65382 6.40559H9.74582L9.38382 9.67859H6.65382V19.1726Z" fill="white" />
                      </svg>
                    </a>
                    <a className="footer-top-item" href="">
                      <svg width="23.75" height="18.75" viewBox="0 0 19 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path opacity="1" className="twitter" d="M18.3981 2.41449C17.7231 2.70013 17.0072 2.88765 16.2741 2.97084C17.0365 2.53297 17.623 1.84099 17.899 1.01526C17.1731 1.42694 16.3789 1.71699 15.5508 1.87286C14.8519 1.15947 13.8744 0.755389 12.8519 0.757085C10.8105 0.757085 9.15361 2.33909 9.15361 4.29061C9.15361 4.5679 9.18627 4.83816 9.25021 5.09623C6.17639 4.94862 3.45216 3.54236 1.62787 1.404C1.30934 1.92529 1.12764 2.53297 1.12764 3.18076C1.12764 4.40623 1.78024 5.48805 2.77231 6.12188C2.18494 6.10468 1.61046 5.95309 1.09706 5.67983V5.7249C1.09706 7.43659 2.37189 8.86495 4.06278 9.19027C3.5182 9.3312 2.94722 9.35187 2.39305 9.25072C2.86384 10.6549 4.22998 11.6764 5.8475 11.7061C4.5821 12.6524 2.98781 13.2184 1.25483 13.2184C0.960153 13.2181 0.66574 13.2015 0.373047 13.1689C2.00943 14.1713 3.95365 14.7571 6.04185 14.7571C12.8438 14.7571 16.5627 9.37118 16.5627 4.70056C16.5627 4.54779 16.5627 4.39502 16.5513 4.24335C17.2749 3.74295 17.8998 3.12372 18.3969 2.41449H18.3981Z" fill="white" />
                      </svg>
                    </a>
                    <a className="footer-top-item" href="">
                      <svg width="26" height="19" viewBox="0 0 26 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path className="youtube" d="M20.61 0.183594H5.98961C3.12627 0.183594 0.799805 2.55085 0.799805 5.4644V12.8973C0.799805 15.8163 3.12627 18.1781 5.98961 18.1781H20.6046C23.4733 18.1781 25.7944 15.8108 25.7944 12.8973V5.4644C25.7998 2.55085 23.4733 0.183594 20.61 0.183594ZM17.0959 9.54779L10.2575 12.8642C10.0731 12.9524 9.86705 12.82 9.86705 12.6103V5.7679C9.86705 5.55821 10.084 5.42578 10.2629 5.51958L17.1013 9.04564C17.3074 9.15048 17.302 9.44846 17.0959 9.54779Z" fill="#FFF" />
                      </svg>
                    </a>
                    <a className="footer-top-item" href="">
                      <svg width="22.5" height="21.375" viewBox="0 0 20 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path className="twitch" fillRule="evenodd" clipRule="evenodd" d="M19.9602 13.6252V9.5538V0.680176H2.93894L0.811279 2.97688V15.0868H6.13043V18.845H8.47085L11.024 16.1307H17.407L19.9602 13.6252ZM7.40694 13.4165H4.00268C3.76864 13.4165 3.57715 13.2286 3.57715 12.9989V2.76817C3.57715 2.5385 3.76864 2.35059 4.00268 2.35059H17.6197C17.8537 2.35059 18.0452 2.5385 18.0452 2.76817V10.7022C18.0452 10.8171 18.0027 10.9215 17.9229 10.9998L15.5825 13.2965C15.5027 13.3748 15.391 13.4165 15.2793 13.4165H11.3697L8.07183 15.6401C8.00268 15.6871 7.91757 15.7132 7.83247 15.7132C7.76332 15.7132 7.69949 15.6976 7.63566 15.6662C7.49204 15.5932 7.40694 15.4522 7.40694 15.2956V13.4165Z" fill="white" />
                        <path d="M13.79 9.76274V5.58691H12.0878V9.76274H13.79ZM9.53465 9.76274V5.58691H7.83252V9.76274H9.53465Z" fill="white" />
                      </svg></a>
                  </div>
                </div>
                <div className="footer-bottom">
                  <p className="copyright">© 2018 Your Games, Inc. All Right Reserved</p>
                  <div className="footer-info">
                    <div className="info-item">
                      <a href=""><p>PRIVACY POLICY</p></a>
                    </div>
                    <div className="info-item border-info">
                      <a href=""><p>TERM OF SERVICES</p></a>
                    </div>
                    <div className="info-item border-info">
                      <a href=""><p>CODE OF CONDUCT</p></a>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </section>
      </>
      </>

      )}
     
  

export default LandingPage;
