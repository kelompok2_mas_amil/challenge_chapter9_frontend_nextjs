import React, { useState, useEffect } from "react";
import logo from '../../public/image/logo.png';
import kembali from '../../public/image/refresh.png';
import rock from '../../public/image/batu.png';
import scissor from '../../public/image/gunting.png';
import paper from '../../public/image/kertas.png';
import axios from "axios";
import Image from "next/image";
import { useRouter } from "next/router";

export default function Index() {
    const router = useRouter();
    /* const [action, setAction] = useState('');
    const [pscore, setPscore] = useState(0);
    const [cscore, setCscore] = useState(0);
    const [status, setStatus] = useState('');
    const [comstyle, setComstyle] = useState('');

    const rockIcon = "rock";
    const paperIcon = "paper";
    const scissorsIcon = "scissors";
    const randomClasses = [rockIcon, paperIcon, scissorsIcon];
    const randomNum = action && Math.floor(Math.random() * randomClasses.length);
    const computerShowIcon = randomClasses[randomNum];

    const clickStyle = () => {
        setAction('');
        setComstyle('');
    }

    const token = sessionStorage.getItem("accessToken");
    const id = sessionStorage.getItem("id");
    const url = 'http://localhost:8080/api/add-score/'+id;
    console.log(id)
    function updateScore(score) {
        axios.put(url, {
            'score': score
        }, {
            headers: {
                'Authorization': token
            }
        }).then(response => console.log(response));
    }

    const actionGame = () => {
        if (action === 'rock' && computerShowIcon === scissorsIcon) {
            let score = pscore + 1;

            updateScore(score)
            setPscore(score);

            setStatus('PLAYER 1 WIN');
            setComstyle(scissorsIcon);
        } else if (action === 'rock' && computerShowIcon === paperIcon) {
            setCscore(cscore + 1);
            setStatus('COM WIN');
            setComstyle(paperIcon);
        } else if (action === 'paper' && computerShowIcon === scissorsIcon) {
            setCscore(cscore + 1);
            setStatus('COM WIN');
            setComstyle(scissorsIcon);
        } else if (action === 'paper' && computerShowIcon === rockIcon) {
            let score = pscore + 1;

            updateScore(score)
            setPscore(score);

            setStatus('PLAYER 1 WIN');
            setComstyle(rockIcon);
        } else if (action === 'scissors' && computerShowIcon === rockIcon) {
            setCscore(cscore + 1);
            setStatus('COM WIN');
            setComstyle(rockIcon);
        } else if (action === 'scissors' && computerShowIcon === paperIcon) {
            let score = pscore + 1;

            updateScore(score);
            setPscore(score);

            setStatus('PLAYER 1 WIN');
            setComstyle(paperIcon);
        } else if (action === computerShowIcon) {
            setStatus('DRAW');
            setComstyle(computerShowIcon);
        } else {
            setStatus('VS');
        }
    }
    useEffect(() => {
        actionGame();
        if (parseInt(pscore) + parseInt(cscore) === 10) {
            parseInt(pscore) > parseInt(cscore) ? alert("PLAYER WIN") : alert("COM WIN");
            setPscore(0);
            setCscore(0);
            setStatus('VS');
        }
    }, [action]) */

    return (
        <div className="body">
            <header className="header">
                <div className="arrow-back" onClick={() => router.push("/games/detail")}>
                    &lt;
                </div>
                <div className="brand">
                    <div className="logoI" style={{ position: 'relative', width: '72px', height: '72px' }}>
                        <Image src='/image/logo.png' layout="fill" objectFit="contain" alt="logo" />
                    </div>
                    <h1>ROCK PAPER SCISSORS</h1>
                </div>
            </header>
            <section className="slide1">
                {/* <div className="user-choice">
                    <h1 className="player-name">PLAYER 1</h1>
                    <h1>{pscore}</h1>
                    <div className="user">
                        <button disabled={action} onClick={() => setAction('rock')} className={(action === 'rock') ? 'activee' : ''}>
                            <img className="rock" src={rock} alt="batu" />
                        </button>
                        <button disabled={action} onClick={() => setAction('paper')} className={(action === 'paper') ? 'activee' : ''}>
                            <img className="paper" src={paper} alt="kertas" />
                        </button>
                        <button disabled={action} onClick={() => setAction('scissors')} className={(action === 'scissors') ? 'activee' : ''}>
                            <img className="scissors" src={scissor} alt="gunting" />
                        </button>
                    </div>
                </div>
                <div className="vs">
                    <h1 className={status !== 'VS' ? 'action' : ''}>{status}</h1>
                    <img onClick={() => clickStyle()} className="refresh" src={kembali} alt="batu" />
                </div>
                <div className="com-choice">
                    <h1 className="player-com">COM</h1>
                    <h1>{cscore}</h1>
                    <div className="com">
                        <button className={(comstyle === rockIcon) ? 'activee' : ''}>
                            <img className="rock" src={rock} alt="batu" />
                        </button>
                        <button className={(comstyle === paperIcon) ? 'activee' : ''}>
                            <img className="paper" src={paper} alt="kertas" />
                        </button>
                        <button className={(comstyle === scissorsIcon) ? 'activee' : ''}>
                            <img className="scissors" src={scissor} alt="gunting" />
                        </button>
                    </div>
                </div> */}
            </section>
        </div>
    )
}