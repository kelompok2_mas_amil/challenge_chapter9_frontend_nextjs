import { useEffect, useState } from "react";
import axios from 'axios';
import { useRouter } from "next/router";
import Head from 'next/head'
import Image from "next/image";

export default function GameList() {
    const router = useRouter();
    return (
        <div className="boxs">
            <section
                onClick={() => router.push('/games/detail')}>
                <div className="box">
                    <p className="textJ">Rock, Paper, Scissor</p>
                    <div className="logoJ">
                        <Image src='/image/game.png' layout="fill" alt="boy" objectFit="contain" />
                    </div>
                </div>
            </section>
            <section
                onClick={() => router.push('/games/detail')}>
                <div className="box">
                    <p className="textJ">Ludo</p>
                    <div className="logoJ">
                        <Image src='/image/game-ludo.jpeg' layout="fill" alt="boy" objectFit="contain" />
                    </div>
                </div>
            </section>
        </div>
    )
}
