import React, { useEffect, useState } from "react";
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Link from "next/link";
import { useRouter } from "next/router";
import Image from "next/image";

export default function Detail() {
    const foto = '/images/main-bg.jpg';
    const [score, setScore] = useState([]);
    const [items, setItems] = useState([]);
    const [token, setToken] = useState([]);
    const router = useRouter();

    // const notify = React.useCallback((type, message) => {
    //     toast({ type, message });
    // }, []);

    const notify = () => toast.success("Wow so easy!");


    useEffect(() => {
        axios.get('http://localhost:8080/api/users').then(res => {
            setItems(res.data);
            // console.log(res.data);
            // setScore(res.data.scores);
        });

        const token = sessionStorage.getItem("accessToken");
        setToken(token);

    }, []);

    function checkToken() {
        notify();
        if (token !== null) {
            console.error('play games');
        } else {
            console.error('play games');
            // ToastMessage({ type: 'info', message: 'play games' });
        }
    }

    return (
        <div className="containerBatu">
            <div className="row">
                <div className="col-xl-6 mb-5 kiri">
                    <div>
                        <h3 className="gamedetails_texth3">Rock, Paper, Scissor</h3>
                        <p className='gamedetails_textp'>
                            A classic two-person game. Players start each round by click, “rock, paper, scissors!” Rock crushes scissors, scissors cut paper, and paper covers rock. See who wins each round!
                        </p>
                    </div>
                    <div className="bungkus">
                        <p className="high_p">Highscore Top 3</p>
                        <table className='table_leaderboard table-hover table-striped table-bordered'>
                            <thead>
                                <tr className='tr'>
                                    <th className='th'>
                                        <p>Name</p>
                                    </th>
                                    <th className='th'>
                                        <p>Score</p>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                {items.sort((a, b) => b.total_score - a.total_score).map((item, index) => (
                                    <tr key={index} className='tr' data-index={index}>
                                        <td className='td'>
                                            <Link href={`/profile/{item.id}`}>
                                                {item.username}
                                            </Link>
                                        </td>
                                        <td className='td'>{item.total_score ?? 0}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                    <div className="b_play d-grid  ">
                        {/* <button className="play btn btn-primary" type="button" onClick={() => checkToken()}>Play Now</button> */}
                        <button className="play btn btn-primary" type="button" onClick={() => router.push('/games')}>Play Now</button>
                    </div>
                </div>
                <br />
                <div className="col-xl-6 mb-5 kanan">
                    <div style={{ width: '100%', height: '100%', position: 'relative' }}>
                        <Image className="bkg" src={foto} alt="foto" layout="fill" objectFit="fill" />
                    </div>
                </div>
            </div>
        </div>
    )
}