import Link from 'next/link';
import styles from '../../styles/Login.module.css';

export default function Login() {
    return (
        <>
            <div className={styles.backgroundLogin}></div>
            <form className={styles.input}>
                <div className={styles.inputContainer}>
                    <h1>LOGIN</h1>
                    
                    <div className={styles.inputBox}>
                        <input type="text"required="required" name="email"onChange={(value)=>setEmail(value.target.value)}/>
                        <span>email</span>
                    </div>
                    
                    <div className={styles.inputBox}>
                        <input type="password"required="required" name="password"onChange={(value)=>setPassword(value.target.value)}/>
                        <span>Password</span>
                    </div>

                    <h5 className={styles.error}>&nbsp;</h5>
                    <button className={styles.buttonLogin} onClick={ (e) => handleSubmit(e) }>LOGIN</button>
                    <Link className={styles.loginNav} href="/register">Sign Up</Link>
                    <Link className={styles.loginNav} href="/">Back</Link>
                </div>
            </form>
        </>
    )
}